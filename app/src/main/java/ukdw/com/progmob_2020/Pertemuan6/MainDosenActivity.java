package ukdw.com.progmob_2020.Pertemuan6;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import ukdw.com.progmob_2020.Crud.HapusMhsActivity;
import ukdw.com.progmob_2020.Crud.MahasiswaAddActivity;
import ukdw.com.progmob_2020.Crud.MahasiswaGetAllActivity;
import ukdw.com.progmob_2020.Crud.MahasiswaUpdateActivity;
import ukdw.com.progmob_2020.Crud.MainMhsActivity;
import ukdw.com.progmob_2020.R;

public class MainDosenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_dosen);

        //variable
        Button buttonGetDosen = (Button)findViewById(R.id.buttonGetDosen);
        Button buttonAddDosen = (Button)findViewById(R.id.buttonAddDosen);
        Button buttonUpdateDosen = (Button)findViewById(R.id.buttonUpdateDosen);
        Button buttonDelDosen = (Button)findViewById(R.id.buttonDelDosen);

        //action
        buttonGetDosen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                Intent intent = new Intent(MainDosenActivity.this, DosenGetAllActivity.class);
                startActivity(intent);
            }
        });
        buttonAddDosen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                Intent intent = new Intent(MainDosenActivity.this, DosenAddActivity.class);
                startActivity(intent);
            }
        });
        buttonUpdateDosen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainDosenActivity.this, DosenUpdateActivity.class);
                startActivity(intent);
            }
        });
        buttonDelDosen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainDosenActivity.this, HapusDosenActivity.class);
                startActivity(intent);
            }
        });
    }
}