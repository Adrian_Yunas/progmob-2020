package ukdw.com.progmob_2020.Pertemuan6;

        import androidx.appcompat.app.AppCompatActivity;
        import androidx.recyclerview.widget.LinearLayoutManager;
        import androidx.recyclerview.widget.RecyclerView;

        import android.app.ProgressDialog;
        import android.os.Bundle;
        import android.widget.Toast;

        import java.util.List;

        import retrofit2.Call;
        import retrofit2.Callback;
        import retrofit2.Response;
        import ukdw.com.progmob_2020.Adapter.MatkulCRUDRecyclerAdapter;
        import ukdw.com.progmob_2020.Model.Matkul;
        import ukdw.com.progmob_2020.Network.GetDataService;
        import ukdw.com.progmob_2020.Network.RetrofitClientInstance;
        import ukdw.com.progmob_2020.R;

public class MatkulGetAllActivity extends AppCompatActivity {

    RecyclerView rvMtk;
    MatkulCRUDRecyclerAdapter mtkAdapter;
    ProgressDialog pd;
    List<Matkul> matkulList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_matkul_get_all);

        rvMtk = (RecyclerView)findViewById(R.id.rvGetMatkulAll);
        pd = new ProgressDialog(this);
        pd.setTitle("Mohon Bersabar");
        pd.show();

        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<List<Matkul>> call = service.getMatkul("72180183");

        call.enqueue(new Callback<List<Matkul>>() {
            @Override
            public void onResponse(Call<List<Matkul>> call, Response<List<Matkul>> response) {
                pd.dismiss();
                matkulList = response.body();
                mtkAdapter = new MatkulCRUDRecyclerAdapter(matkulList);

                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(MatkulGetAllActivity.this);
                rvMtk.setLayoutManager(layoutManager);
                rvMtk.setAdapter(mtkAdapter);
            }

            @Override
            public void onFailure(Call<List<Matkul>> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(MatkulGetAllActivity.this,"Error", Toast.LENGTH_LONG).show();
            }
        });
    }
}