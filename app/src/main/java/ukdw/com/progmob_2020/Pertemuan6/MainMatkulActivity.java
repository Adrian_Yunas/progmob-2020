package ukdw.com.progmob_2020.Pertemuan6;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import ukdw.com.progmob_2020.Model.Matkul;
import ukdw.com.progmob_2020.R;

public class MainMatkulActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_matkul);

        //variable
        Button buttonGetMatkul = (Button)findViewById(R.id.buttonGetMatkul);
        Button buttonAddMatkul = (Button)findViewById(R.id.buttonAddMatkul);
        Button buttonUpdateMatkul = (Button)findViewById(R.id.buttonUpdateMatkul);
        Button buttonDelMatkul = (Button)findViewById(R.id.buttonDelMatkul);

        //action
        buttonGetMatkul.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                Intent intent = new Intent(MainMatkulActivity.this, MatkulGetAllActivity.class);
                startActivity(intent);
            }
        });
        buttonAddMatkul.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                Intent intent = new Intent(MainMatkulActivity.this, MatkulAddActivity.class);
                startActivity(intent);
            }
        });
        buttonUpdateMatkul.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainMatkulActivity.this, MatkulUpdateActivity.class);
                startActivity(intent);
            }
        });
        buttonDelMatkul.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainMatkulActivity.this, HapusMatkulActivity.class);
                startActivity(intent);
            }
        });
    }
}